#include <pitchToFrequency.h>
#include <pitchToNote.h>
#include <MIDIUSB.h>
#include <frequencyToNote.h>
#include <MIDIUSB_Defs.h>

struct buttonState {
  uint8_t pin;
  unsigned long lastDebounceTime;
  int lastButtonState;
  int buttonState;
};

unsigned long debounceDelay = 5;    // the debounce time; increase if the output flickers

//const uint8_t PEDAL_BUTTON = 8;
//const uint8_t TRACK_BUTTON = 9;

buttonState PEDAL_BUTTON = {8, 0, HIGH, HIGH};
buttonState TRACK_BUTTON = {9, 0, HIGH, HIGH};

const byte MIDI_CHANNEL = 0x00;  // Use midi channel 1
const byte MIDI_VALUE_ON = 127;
const byte MIDI_VALUE_OFF = 0;


const byte MIDI_RECORD_CODE[4]  = {64, 65, 66, 67};
const byte MIDI_PLAY_CODE[4] = {70, 71, 72, 73};
const byte MIDI_DELETE_CODE[4] = {74, 75, 76, 77};

byte currentTrack = 0;
boolean isRecording = false;
boolean isPlaying = false;
int ledState = LOW;         // the current state of the output pin


// Variables will change:
//int buttonState;             // the current reading from the input pin
//int lastButtonState[2] = {HIGH, HIGH};   // the previous reading from the input pin

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
//unsigned long lastDebounceTime[2] = {0, 0};  // the last time the output pin was toggled


void setup() {
  Serial.begin(9600);
  //while (!Serial); // Arduino Leonardo serial fix
  Serial.println("MIDI device started!");

  pinMode(LED_BUILTIN, OUTPUT);

  pinMode(PEDAL_BUTTON.pin, INPUT_PULLUP);
  pinMode(TRACK_BUTTON.pin, INPUT_PULLUP);
}

void loop() {
  loopButtonDebounce(&TRACK_BUTTON,
  []() {
    currentTrack++;
    if (currentTrack >= 4) {
      currentTrack = 0;
    }

    Serial.println("Current track: ");
    Serial.println(currentTrack);
  });

  loopButtonDebounce(&PEDAL_BUTTON,
  []() {
    if (isRecording) {
      // Stop recording
      controlChange(MIDI_CHANNEL, MIDI_RECORD_CODE[currentTrack], MIDI_VALUE_OFF);
      // Play track
      controlChange(MIDI_CHANNEL, MIDI_PLAY_CODE[currentTrack], MIDI_VALUE_ON);
      MidiUSB.flush();
      isPlaying = true;
      isRecording = false;
      ledState = LOW;
    } else {
      controlChange(MIDI_CHANNEL, MIDI_PLAY_CODE[currentTrack], MIDI_VALUE_OFF);
      controlChange(MIDI_CHANNEL, MIDI_DELETE_CODE[currentTrack], MIDI_VALUE_ON);
      controlChange(MIDI_CHANNEL, MIDI_RECORD_CODE[currentTrack], MIDI_VALUE_ON);
      MidiUSB.flush();
      isRecording = true;
      ledState = HIGH;
    }
  });

  digitalWrite(LED_BUILTIN, ledState);
}



void loopButtonDebounce(buttonState *button, void (*whenPressed)()) {
  loopButtonDebounce(button, *whenPressed, NULL);
}

void loopButtonDebounce(buttonState *button, void (*whenPressed)(), void (*whenNotPressed)()) {

  // TODO delays for each button
  int reading = digitalRead(button->pin);

  if (reading != button->lastButtonState) {
    // reset the debouncing timer
    button->lastDebounceTime = millis();
  }

  if ((millis() - button->lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != button->buttonState) {
      button->buttonState = reading;

      if (button->buttonState == LOW) {
        if (whenPressed != NULL) {
          whenPressed();
        }
      } else {
        if (whenNotPressed != NULL) {
          whenNotPressed();
        }
      }
    }
  }

  // save the reading. Next time through the loop, it'll be the lastButtonState:
  button->lastButtonState = reading;
}

void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}
